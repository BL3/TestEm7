# BL3 TestEm7 Simulations

This repository contains the extended/TestEm7 example
and physics list from geant4. It has been modified to study the
energy deposition and backscattering of 30 keV protons on silicon
detectors to allow for comparisons with SRIM.

## Downloading

Download the code by cloning this repository:
```
git clone git@gitlab.com:BL3/TestEm7.git
```
This will create a directory `TestEm7` with the code.

## Compilation

To compile in a directory `build`, use the following commands
(from inside the `TestEm7` directory):
```
mkdir build
cmake -B build .
make -C build
```

## Running

To run the simulation with the example `snr.mac` macro, use
the following commands
```
build/TestEm7 snr.mac
```
This will generate a few (depending on the number of cores) root
files named `TestEm7_t0.root` etc.

## Analyzing

To analyze, you may be interested in the plot.C macro:
```
root plot.C
```
This will `hadd` the different root files together and plot a few
useful (or not so useful) histograms.
