//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file electromagnetic/TestEm7/src/SteppingAction.cc
/// \brief Implementation of the SteppingAction class
//
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "SteppingAction.hh"
#include "G4Step.hh"
#include "G4StepPoint.hh"
#include "DetectorConstruction.hh"
#include "RunAction.hh"
#include "Randomize.hh"
#include "G4VProcess.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::SteppingAction(DetectorConstruction* det, RunAction* RuAct)
:G4UserSteppingAction(),fDetector(det), fRunAction(RuAct)
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::~SteppingAction()
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SteppingAction::UserSteppingAction(const G4Step* step)
{
  G4double edep = step->GetTotalEnergyDeposit();

  G4StepPoint* prePoint  = step->GetPreStepPoint();
  G4StepPoint* postPoint = step->GetPostStepPoint();

  G4int copyNb = prePoint->GetTouchableHandle()->GetCopyNumber();
  if (copyNb > 0) { fRunAction->FillTallyEdep(copyNb-1, edep); }

  G4double niel = step->GetNonIonizingEnergyDeposit();
  fRunAction->FillEdep(edep, niel);
  
  if (step->GetTrack()->GetTrackID() == 1) {
    fRunAction->AddPrimaryStep();
    /*
    G4cout << step->GetTrack()->GetMaterial()->GetName()
           << "  E1= " << step->GetPreStepPoint()->GetKineticEnergy()
           << "  E2= " << step->GetPostStepPoint()->GetKineticEnergy()
           << " Edep= " << edep 
           << " Q= " << step->GetTrack()->GetDynamicParticle()->GetCharge()
           << " Qp= " << step->GetPostStepPoint()->GetCharge()
           << G4endl;
    */
  } 

  // get analysis manager
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

  //Bragg curve
  //
  G4double xmax = fDetector->GetAbsorSizeX();

  G4double x1 = prePoint->GetPosition().x() + xmax*0.5;
  G4double x2 = postPoint->GetPosition().x() + xmax*0.5;
  if(x1 >= 0.0 && x2 <= xmax) {
    G4double x  = x1 + G4UniformRand()*(x2-x1);
    if (step->GetTrack()->GetDefinition()->GetPDGCharge() == 0.) x = x2;
    analysisManager->FillH1(1, x, edep);
    analysisManager->FillH1(2, x, edep);
  }

  G4int proc = 0;
  G4int pid = step->GetTrack()->GetDynamicParticle()->GetPDGcode();
  if (step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName()=="msc")                        proc =10;
  if (step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName()=="e-_G4MicroElecElastic")      proc =11;
  if (step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName()=="e-_G4MicroElecInelastic")    proc =12;
  if (step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName()=="eCapture")                   proc =13;
  if (step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName()=="p_G4MicroElecInelastic")     proc =14;
  if (step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName()=="ion_G4MicroElecInelastic")   proc =15;
  if (step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName()=="hIoni")                      proc =16;
  if (step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName()=="eIoni")                      proc =17;
  if (step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName()=="Transportation")             proc =20;
  {
    G4double t0 = step->GetPreStepPoint()->GetGlobalTime()/nanosecond;
    G4double x0 = step->GetPreStepPoint()->GetPosition().x()/nanometer;
    G4double y0 = step->GetPreStepPoint()->GetPosition().y()/nanometer;
    G4double z0 = step->GetPreStepPoint()->GetPosition().z()/nanometer;
    G4double E0 = step->GetPreStepPoint()->GetKineticEnergy()/electronvolt;
    G4double t1 = step->GetPostStepPoint()->GetGlobalTime()/nanosecond;
    G4double x1 = step->GetPostStepPoint()->GetPosition().x()/nanometer;
    G4double y1 = step->GetPostStepPoint()->GetPosition().y()/nanometer;
    G4double z1 = step->GetPostStepPoint()->GetPosition().z()/nanometer;
    G4double E1 = step->GetPostStepPoint()->GetKineticEnergy()/electronvolt;
    G4double px = step->GetPostStepPoint()->GetMomentumDirection().x();
    G4double py = step->GetPostStepPoint()->GetMomentumDirection().y();
    G4double pz = step->GetPostStepPoint()->GetMomentumDirection().z();

    // fill ntuple
    int i = 0;
    analysisManager->FillNtupleDColumn(i++, proc);
    analysisManager->FillNtupleDColumn(i++, pid);
    analysisManager->FillNtupleDColumn(i++, t0);
    analysisManager->FillNtupleDColumn(i++, x0);
    analysisManager->FillNtupleDColumn(i++, y0);
    analysisManager->FillNtupleDColumn(i++, z0);
    analysisManager->FillNtupleDColumn(i++, E0);
    analysisManager->FillNtupleDColumn(i++, t1);
    analysisManager->FillNtupleDColumn(i++, x1);
    analysisManager->FillNtupleDColumn(i++, y1);
    analysisManager->FillNtupleDColumn(i++, z1);
    analysisManager->FillNtupleDColumn(i++, E1);
    analysisManager->FillNtupleDColumn(i++, px);
    analysisManager->FillNtupleDColumn(i++, py);
    analysisManager->FillNtupleDColumn(i++, pz);
    analysisManager->FillNtupleDColumn(i++, step->GetTotalEnergyDeposit()/eV);
    analysisManager->FillNtupleDColumn(i++, std::sqrt((x1-x0)*(x1-x0)+(y1-y0)*(y1-y0)+(z1-z0)*(z1-z0)));
    analysisManager->FillNtupleDColumn(i++, (E0 - E1));
    analysisManager->AddNtupleRow();
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


